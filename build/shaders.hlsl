static const float3 positions[3] = {
	float3(0.0f, 1.0f, 0.0f),
	float3(1.0f, -1.0f, 0.0f),
	float3(-1.0f, -1.0f, 0.0f)
};

static const float3 colors[3] = {
	float3(1.0f, 0.0f, 0.0f),
	float3(0.0f, 1.0f, 0.0f),
	float3(0.0f, 0.0f, 1.0f)
};

struct VSOutput
{
	float4 position : SV_POSITION;
	float3 color : COLOR;
};

VSOutput VSMain(uint id : SV_VertexID)
{
	VSOutput vsOut;
	vsOut.position = float4(positions[id].xyz, 1.0f);
	vsOut.color = colors[id];
	return vsOut;
}
float4 PSMain(VSOutput psInput) : SV_TARGET
{
	return float4(psInput.color.rgb, 1.0f);
}