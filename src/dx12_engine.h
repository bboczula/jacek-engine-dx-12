#pragma once

#include <iostream>
#include <dxgi1_5.h>
#include <d3d12.h>

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "D3DCompiler.lib")

class SDL_Window;
class ID3D12Device;
class ID3D12CommandQueue;
class ID3D12CommandAllocator;
class ID3D12GraphicsCommandList;
class ID3D12DescriptorHeap;
class ID3D12Resource;
class ID3D12Fence;

template<typename T>
void Log(T arg)
{
  std::cout << arg << '\n';
}

template<typename... Types>
void Log(Types const&... args)
{
  (std::cout << ... << AddSpace(args)) << '\n';
}

template<typename T>
void SafeRelease(T* comObject)
{
  if (comObject)
  {
    comObject->Release();
    comObject = nullptr;
  }
}

template<typename T>
class AddSpace
{
private:
  T const& ref;
public:
  AddSpace(T const& r) : ref(r)
  {
  }
  friend std::ostream& operator<< (std::ostream& os, AddSpace<T> s)
  {
    return os << s.ref << ' ';
  }
};

template<typename T, unsigned int Size>
struct TStaticArray
{
  T array[Size];
};

template<unsigned int Width, unsigned int Height>
struct TResolution
{
  unsigned int width = Width;
  unsigned int height = Height;
};

class Dx12Engine
{
public:
  // Initializes everything in the engine
  void init();

  // Shuts down the engine
  void cleanup();

  // Draw loop
  void draw();

  void ExecuteCommandList();

  void RecordCommandList();

  // Run main loop
  void run();
private:
  void InitializeSdl();
  void CreateDxgiAndEnumAdapters();
  void CreateDxDevice();
  void CreateCommandQueue();
  void CreateSwapChain();
  void CreateCommandAllocator();
  void CreateCommandList();
  void CreateDescriptorHeap();
  void CreateFrameResources();
  void WaitForPreviousFrame();
  void CreateFence();
  void CreateRootSignature();
  void CreatePipelineState();
  HWND hwnd;
  UINT rtv_descriptor_size_;
  UINT frame_index_ = { 0 };
  UINT64 fence_value_;
  HANDLE fence_event_;
  D3D12_VIEWPORT viewport_;
  D3D12_RECT scissor_rect_;
  SDL_Window* window_;
  IDXGIFactory5* dxgi_factory_;
  IDXGIAdapter1* dxgi_adapter_;
  IDXGISwapChain3* dxgi_swap_chain_;
  ID3D12Device* device_;
  ID3D12CommandQueue* command_queue_;
  ID3D12CommandAllocator* command_allocator_;
  ID3D12GraphicsCommandList* command_list_;
  ID3D12DescriptorHeap* rtv_descriptor_heap_;
  ID3D12Fence* fence_;
  ID3D12RootSignature* root_signature_;
  ID3D12PipelineState* pipeline_state_;
  TStaticArray<ID3D12Resource*, 2> render_targets_;
};
