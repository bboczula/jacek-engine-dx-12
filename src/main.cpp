#include "vk_engine.h"
#include "dx12_engine.h"

int main(int argc, char* argv[])
{
	//VulkanEngine engine;
    Dx12Engine engine;

	engine.init();	
	
	engine.run();	

	engine.cleanup();	

	return 0;
}
